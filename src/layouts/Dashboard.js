import React, { useContext, useState } from "react";
import { Container, Row, Col, Navbar, NavDropdown, Nav } from "react-bootstrap";
import Home from "../components/Home";
import { useRoutes, Navigate, Link } from "react-router-dom";
import { UserService } from "../services/UserService";
import { LoginCallbackWithRouter } from "../components/LoginCallback";
import { useTranslation } from "react-i18next";
import { ApplicationContext } from '../ApplicationContext';
import StudentDashboard from '../components/StudentDashboard'
import { GradesAll } from '../components/grades/GradesAll';
import { GradesTheses } from "../components/grades/GradesTheses";
import { LogoutWithRouter } from '../components/Logout';
import { Language } from '../components/Language';
import { Grades } from "../components/grades/Grades";
import { GradesRecent } from "../components/grades/GradesRecent";

const Dash = () => {
  const { t, i18n } = useTranslation();

  const { configuration, context } = useContext(ApplicationContext);
  const userService = new UserService(configuration, context);
  const [user, setUser] = useState(userService.user);
  
  const formatRoutes = (routes) => {
    return routes.map((route) => {
      if (route.authGuard && !user) {
        route.element = <Navigate to="/" />;
      }
      return route;
    });
  }

  const routes = useRoutes(formatRoutes([
    {
      path: "/auth/callback",
      element: <LoginCallbackWithRouter onUserChange={setUser} />
    },
    {
      path: "/auth/logout",
      element: <LogoutWithRouter />
    },
    {
      path: "/",
      exact: true,
      element: <Home />,
    },
    {
      path: "/dashboard",
      authGuard: true,
      element: <StudentDashboard />
    },
    {
      path: "/grades",
      authGuard: true,
      element: <Grades />,
      children: [
        {
          path: "recent",
          element: <GradesRecent />
        },
        {
          path: "all",
          element: <GradesAll />
        },
        {
          path: "theses",
          element: <GradesTheses />
        }
      ]
    },
  ]));

  return (
    <>
      <Navbar bg="light" expand="lg">
        <Navbar.Brand as={Link} to="/">Students App</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link as={Link} to="/">{t("Home")}</Nav.Link>
          </Nav>
          <Nav>
            {
              (user != null) ?
                (
                  <>
                    <Nav.Link href="#">{t("Registrations")}</Nav.Link>
                    <NavDropdown title={t("Grades")} id="grades-dropdown">
                      <NavDropdown.Item as={Link} to={"/grades/recent"}>{t("Recent Grades")}</NavDropdown.Item>
                      <NavDropdown.Item as={Link} to={"/grades/all"}>{t("All Grades")}</NavDropdown.Item>
                      <NavDropdown.Item as={Link} to="/grades/theses">{t("Theses Grades")}</NavDropdown.Item>
                    </NavDropdown>
                    <NavDropdown title={user.name} id="user-dropdown">
                      <NavDropdown.Item as={Link} to="/auth/logout">
                        {t("Logout")}
                      </NavDropdown.Item>
                    </NavDropdown>
                  </>
                ) :
                <Nav.Link as={Link} to={userService.loginURI}>
                  {t("Login")}
                </Nav.Link>
            }
            <Language />
          </Nav>
        </Navbar.Collapse>
      </Navbar>
      <Container fluid>
        <Row>
          <Col className="pt-3" xs={12} id="page-content-wrapper">
            {routes}
          </Col>
        </Row>
      </Container>
    </>
  );
};

const Dashboard = Dash;
export default Dashboard;

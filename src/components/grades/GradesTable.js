import { faChartLine, faChevronDown, faChevronUp, faFlagCheckered, faUserSecret } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import clsx from "clsx";
import { useRef, useState } from "react";
import { useTranslation } from "react-i18next";

const displayGrade = (course) => {
  const grade = course?.formattedGrade;
  // convert string grade to num
  const numGrade = parseFloat(grade);
  // if grade is not a number then return
  if (!grade || isNaN(numGrade) || numGrade < 0) {
    return null;
  }

  return (
    <span className={clsx(course.isPassed ? 'text-success' : 'text-secondary')}>
      {grade}{" "}
      <FontAwesomeIcon icon={faChartLine} className="font-sm" />
    </span>
  );
}

export function GradesTable({ semester, courseType, courses, passedOnly }) {
  const { t } = useTranslation();

  return (
    <>
      <span className="sis--list-title text-dark">
        {semester ? semester.name : courseType ? courseType.name : t('All Courses')}
      </span>
      <div className="flex-column d-none d-md-block mb-3">
        <DesktopView courses={courses} passedOnly={passedOnly} />
      </div>
      <div className="d-sm-block d-md-none">
        <MobileView courses={courses} passedOnly={passedOnly} />
      </div>
    </>
  );
}

function NoCoursesFound() {
  const { t } = useTranslation();

  return (
    <div className="sis--list-group sis--list-group-body mt-3">
      <li className="sis--list-group-item">
        <div className="d-flex justify-content-center align-items-center">
          <div className="sis--list-group-item_lg_column text-secondary font-italic">
            {t('No courses found')}
          </div>
        </div>
      </li>
    </div>
  );
}

function DesktopView({ courses, passedOnly }) {
  const { t } = useTranslation();

  if (courses.length === 0 || (passedOnly && courses.filter((x) => x.isPassed).length === 0)) {
    return <NoCoursesFound />
  }

  return (
    <>
      <ul className="sis--list-group sis--list-group-header">
        <li className="sis--list-group-item">
          <div className="d-flex justify-content-center align-items-center">
            <div className="sis--list-group-item_lg_column">
              {t('Course')}
            </div>
            <div className="sis--list-group-item_md_column d-none d-md-block">
              {t('Display Code')}</div>
            <div className="sis--list-group-item_lg_column d-none d-xl-block">
              {t('Instructor')}
            </div>
            <div className="sis--list-group-item_md_column d-none d-lg-block">
              {t('Exam Period')}
            </div>
            <div className="sis--list-group-item_sm_column d-none d-md-block">
              {t('Course Type')}
            </div>
            <div className="sis--list-group-item_sm_column d-none d-md-block">
              {t('CP')}</div>
            <div className="sis--list-group-item_sm_column d-none d-md-block">
              {t('ECTS')}
            </div>
            <div className="sis--list-group-item_sm_column d-none d-md-block">
              {t('Coefficient')}
            </div>
            <div className="sis--list-group-item_md_column">
              {t('Grade')}
            </div>
          </div>
        </li>
      </ul>
      <ul className="sis--list-group sis--list-group-body">
        {courses.map((course) => {
          if (passedOnly && !course.isPassed) {
            return null;
          }
          return (
            <li key={course.id} className="sis--list-group-item">
              <div className="d-flex justify-content-center align-items-center list-color">
                <div className="sis--list-group-item_lg_column">
                  {course?.courseTitle}
                </div>
                <div className="sis--list-group-item_md_column d-none d-md-block">
                  {course?.course?.displayCode}
                </div>
                <div className="sis--list-group-item_lg_column d-none d-xl-block">
                  {course?.gradeExam?.instructors
                    .map((x) => x.instructor?.familyName + " " + x.instructor?.givenName)
                    .join(", ")}
                </div>
                <div className="sis--list-group-item_md_column d-none d-lg-block">
                  {course?.examPeriod?.name}
                </div>
                <div className="sis--list-group-item_sm_column d-none d-md-block">
                  {course?.courseType?.abbreviation}
                </div>
                <div className="sis--list-group-item_sm_column d-none d-md-block">
                  {course?.hours}
                </div>
                <div className="sis--list-group-item_sm_column d-none d-md-block">
                  {course?.ects}
                </div>
                <div className="sis--list-group-item_sm_column d-none d-md-block">
                  {course?.coefficient}
                </div>
                <div className="sis--list-group-item_md_column font-weight-bold font-3xl text-success">
                  {displayGrade(course)}
                </div>
              </div>
            </li>
          );
        })}
      </ul>
      <FooterSection courses={courses} passedOnly={passedOnly} />
    </>
  );
}

function MobileView({ courses, passedOnly }) {
  if (courses.length === 0 || (passedOnly && courses.filter((x) => x.isPassed).length === 0)) {
    return <NoCoursesFound />
  }

  return (
    <>
      {courses.map((course) => {
        if (passedOnly && !course.isPassed) {
          return null;
        }
        return <CourseCardCollapsible course={course} key={course.id} />;
      })}
      <FooterSection courses={courses} passedOnly={passedOnly} />
    </>
  );
}

function FooterSection({ courses, passedOnly }) {
  const { t } = useTranslation();
  return (
    <div className="footer-text text-dark font-weight-normal text-right font-sm mt-3 mr-2">
      <div>
        <span>{t('Total Courses')}</span>: <span className="font-weight-bold">
          {passedOnly ? courses.filter((x) => x.isPassed).length : courses.length}
        </span>
      </div>
      <div className="mt-1">
        <span>{t('Average')}</span>:{" "}
        <span className="font-weight-bold">
          {
            (
              courses.filter((x) => x.isPassed).reduce((a, b) => a + b.grade * 10, 0)
              /
              courses.filter((x) => x.isPassed && x.grade).length
            ).toFixed(2)
          }
        </span>
      </div>
      <div className="mt-1">
        <span>{t('Total ECTS')}</span>:{" "}
        {!passedOnly ? (
          <>
            <span className="font-weight-bold">
              {courses.filter((x) => x.isPassed).reduce((a, b) => a + b.ects, 0)}
            </span>
            /
            <span className="font-weight-bold">
              {courses.reduce((a, b) => a + b.ects, 0)}
            </span>
          </>
        ) : (
          <span className="font-weight-bold">
            {courses.filter((x) => x.isPassed).reduce((a, b) => a + b.ects, 0)}
          </span>
        )}
      </div>
    </div>
  );
}

function CourseCardCollapsible({ course }) {
  const { t } = useTranslation();

  const [show, setShow] = useState(false);
  const collapseElement = useRef(null);
  const collapseButton = useRef(null);

  const handleCollapseButton = (e) => {
    e.preventDefault();
    collapseElement.current.classList.toggle('show');
    setShow(collapseElement.current.classList.contains('show'))
  }

  return (
    <>
      <div className="card mb-2">
        <div className="card-header bg-white border-0 d-flex align-items-center">
          <h5 className="d-flex align-items-center w-100 mb-0 mt-2 text-dark font-weight-normal justify-content-between">
            {course?.courseTitle}
            <span className="font-2xl text-right font-weight-bold text-success">
              {displayGrade(course)}
            </span>
          </h5>
          <button
            className="d-inline-block btn btn-link collapsed float-right text-secondary text-decoration-none"
            type="button"
            ref={collapseButton}
            onClick={(e) => handleCollapseButton(e)}
          >
            <span className="d-none d-sm-inline">
              {show ? t('Less') : t('More')}
            </span>
            <FontAwesomeIcon icon={show ? faChevronUp : faChevronDown} className="ml-3 pt-1" />
          </button>
        </div>
        <div className="collapse" ref={collapseElement}>
          <div className="card-body pt-2">
            <div className="row pl-3 pr-4 font-sm">
              <span className="text-secondary mr-3">
                <FontAwesomeIcon icon={faFlagCheckered} className="mr-2" />
                {course?.course?.displayCode}
              </span>
              <span className="text-secondary mr-3">
                <FontAwesomeIcon icon={faUserSecret} className="mr-2" />
                {course?.gradeExam?.instructors
                  .map((x) => x.instructor?.familyName + " " + x.instructor?.givenName)
                  .join(", ")}
              </span>
              <span className="text-secondary mr-3">
                {t('Exam Period')}:{" "}
                {course?.examPeriod?.name}
              </span>
              <span className="text-secondary mr-3">
                {t('Course Type')}:{" "}
                {course?.courseType?.abbreviation}
              </span>
              <span className="text-secondary mr-3">
                {t('CP')}:{" "}
                {course?.hours}
              </span>
              <span className="text-secondary mr-3">
                {t('ECTS')}:{" "}
                {course?.ects}
              </span>
              <span className="text-secondary mr-3">
                {t('Coefficient')}:{" "}
                {course?.coefficient}
              </span>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

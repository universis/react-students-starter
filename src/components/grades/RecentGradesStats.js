import { faFlagCheckered, faUserSecret } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Card } from "react-bootstrap";
import { useTranslation } from "react-i18next";

export function RecentGradesStats({ grades }) {
  const { t } = useTranslation();

  return (
    <Card className="mb-3">
      <Card.Body>
        <div className="d-flex align-items-center">
          <div>
            <Card.Title>{t('Statistics')}</Card.Title>
            <Card.Text>
              <span className="text-muted mr-2">
                <FontAwesomeIcon icon={faFlagCheckered} />{" "}
                {t('Courses')}
              </span>
            </Card.Text>
          </div>
          <div className="ml-auto d-flex align-items-center">
            <span className="font-2xl col-1 text-nowrap text-right font-weight-bold d-sm-block float-right">
              {grades.length}
            </span>
          </div>
        </div>
      </Card.Body>
    </Card>
  );
}

import { Outlet } from "react-router";
import { GradesTabs } from "./GradesTabs";

export function Grades() {
  return (
    <>
      <GradesTabs />
      <Outlet />
    </>
  );
}

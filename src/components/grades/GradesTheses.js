import { useContext, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { GradesService } from "../../services/GradesService";
import { ApplicationContext } from "../../ApplicationContext";
import { Alert, Badge, Card } from "react-bootstrap";
import { Loading } from "../Loading";

export function GradesTheses() {
  const { t } = useTranslation();
  const [theses, setTheses] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  const { context } = useContext(ApplicationContext);
  const gradesService = new GradesService(context);

  const displayGrade = (thesis) => {
    const grade = thesis?.formattedGrade;
    // convert string grade to num
    const numGrade = parseFloat(grade);
    // if grade is not a number then return
    if (!grade || isNaN(numGrade) || numGrade < 0) {
      return "N/A";
    }
    if (thesis.isPassed) {
      return <Badge bg="success">{grade}</Badge>;
    } else {
      return <Badge bg="danger">{grade}</Badge>;
    }
  }

  useEffect(() => {
    gradesService.getThesesInfo().then(res => {
        setTheses(res);
        setError(null);
      })
      .catch(err => {
        setTheses([]);
        setError(err);
      })
      .finally(() => {
        setLoading(false);
      });
  }, []);

  if (loading) return <Loading />;
  return (
    <>
      <h1 className="mb-4">{t("Theses Grades")}</h1>
      {loading && (
        <div>Loading...</div>
      )}
      {error && (
        <Alert variant="danger">
          {error.message}
        </Alert>
      )}
      {!error && !loading && (
        <>
          {theses.length === 0 && (
            <Alert variant="info">
              {t("No theses found")}
            </Alert>
          )}
          {theses.map(x => (
            <Card key={x.id}>
              <Card.Body>
                <Card.Title>{x.thesis.name}</Card.Title>
                <Card.Subtitle className="mb-2">
                  {t("Instructor")}:{" "}
                  {x.thesis.instructor?.familyName}{" "}
                  {x.thesis.instructor?.givenName}
                </Card.Subtitle>
                <Card.Text>
                  {t("Grade")}: {displayGrade(x.thesis)}
                </Card.Text>
              </Card.Body>
            </Card>
          ))}
        </>
      )}
    </>
  );
}
